Meteor.methods({
	'get': function (url) {
		// Set up a future
		var fut = new Future();

		// This should work for any async method
		HTTP.get( url, { /* options */ }, function( error, response) {
				if(response.statusCode==200){
					fut['return'](xml2js.parseStringSync(response.content));		
				}
				else{
					fut['return'](response);		
				} 
			//console.log(response);
		});
		// Wait for async to finish before returning
		// the result
		return fut.wait();

	},
});