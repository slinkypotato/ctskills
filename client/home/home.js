var address = gup('address');

var lat = gup('lat');
var lng = gup('lng');

console.log(address);


function create1(data,cat){
  var img = $('<img class="img-responsive">'); //Equivalent: $(document.createElement('img'))
  ig = getCategoryImage(data,cat);
  if(!ig) return;
  img.attr('src', ig.src);
  // img.attr('height', ig.height);
  // img.attr('width', ig.height);
  img.attr('alt', ig.alt);
  img.css('margin-bottom','10px');
  img.appendTo('#tmp1');


}

function create2(data,cat){
  var img = $('<img class="img-responsive">'); //Equivalent: $(document.createElement('img'))
  ig = getCategoryImage(data,cat);
  if(!ig) return;
  img.attr('src', ig.src);
  // img.attr('height', ig.height);
  // img.attr('width', ig.height);
  img.attr('alt', ig.alt);
  img.css('margin-bottom', '10px');
  img.appendTo('#tmp2');
}

function render1(data){
  myo = getObjects(data);
  $('#del1').css('display','none');
    Session.set('searchResult',getObjects(data));
    create1(myo,"Map");
    create1(myo,"EconomicProperties");
    create1(myo,"QualityOfLife");
    create1(myo,"CompaniesInCity");
    create1(myo,"WeatherPod");
    create1(myo,"ACSEducationEntrainments");
    create1(myo,"HospitalHierarchyInfo");
}

function render2(data){
  myo2 = getObjects(data);
  $('#del2').css('display','none');
      Session.set('localResult',getObjects(data));
      create2(myo2,"Map");
      create2(myo2,"EconomicProperties");
      create2(myo2,"QualityOfLife");
      create2(myo2,"CompaniesInCity");
      create2(myo2,"WeatherPod");
      create2(myo2,"ACSEducationEntrainments");
      create2(myo2,"HospitalHierarchyInfo");

}

Template.home.onCreated(function(){
  Session.set('bAddress','Locating..');
  
Meteor.call('get',"http://api.wolframalpha.com/v2/query?input="+ encodeURI(formatPosition(gup("lat"),gup("lng"))) +"&appid=H3PK3Y-G6UTL84ARX",function(err,data){
    console.log(data);
    console.log(getObjects(data));
    x = getObjects(data);
    result = getCategoryImage(x,"CartographicNearestCity").alt.split("(")[0];
    Session.set('bAddress',result);
     Meteor.call('get',"http://api.wolframalpha.com/v2/query?input="+ encodeURI(result) +"&appid=H3PK3Y-G6UTL84ARX",function(err,data){
      // console.log(data);
      console.log(getObjects(data));
      render2(data);

    }); 

  });

  

  Meteor.call('get',"http://api.wolframalpha.com/v2/query?input="+ encodeURI(gup("address")) +"&appid=KTUR2J-VLQL7YRU6R",function(err,data){
    // console.log(data);
    console.log(getObjects(data));
    render1(data);
  });
  
  
});




Template.home.events({
  'submit .searchBox' (event) {
    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    let target = event.target;
    userAddress = target.text.value;
  },
});

Template.home.helpers({
  getAddress: function() {
    return address;
  },
  bestAddress: function(){
    return Session.get('bAddress');
  }

});

function gup(name) {
  name = RegExp('[?&]' + name.replace(/([[\]])/, '\\$1') + '=([^&#]*)');
  var parameter = (window.location.href.match(name) || ['', ''])[1];
  returnParam = parameter.replace(/%20/g, " ");
  console.log(returnParam);
  return (returnParam);
}

function getCategoryImage(data,cat){
  for(i = 0;i < data.length;i++){
    if(data[i].title == cat){
      return data[i].image;
    }
  }
}


function formatPosition (_lat, _lng){
  if (_lat > 0){
    _lat = _lat + 'N'
  }else{
    _lat = -(_lat) + 'S'
  }
  if (_lng > 0){
    _lng = _lng + 'E'
  }else{
    _lng = -(_lng) + 'W'
  }
  return _lat + ' ' + _lng;
}

function getObjects(data){
  data = data.queryresult;
  pods = data.pod;
  result = [];
  for(i = 1;i < pods.length;i++){
    curr = pods[i];
    // if(!curr.states){
    //  title = "";
    // }
    // else{
    //  title = curr.states[0].state[0].$.input.split(":")[0];
    // }
    title = curr.$.id.split(":")[0];
    image = curr.subpod[0].img[0].$;
    plainText = image.alt;
    result.push({
      title: title,
      image: image,
      plainText: plainText,
    });
  }
  return result;
}
