if (Meteor.isClient) {
    let geoSessionKey = "geoLocation";
    iniUserLocation(geoSessionKey);
    console.log(Session.get(geoSessionKey));

    Template.navbar.events({
        'submit .searchBox' (event) {
            event.preventDefault(); // prevent default browser form submit
            let pos = Session.get(geoSessionKey);
            let searchAddress = event.target.address.value;
            window.location.replace("/home?address=" + searchAddress + "&lat=" + pos.lat + "&lng=" + pos.lng);
        }
    });
    function iniUserLocation(sessionKey) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                let pos = {
                    lat: position.coords.latitude.toFixed(4),
                    lng: position.coords.longitude.toFixed(4)
                };
                Session.set(sessionKey, pos);
            });
        }
    }
}